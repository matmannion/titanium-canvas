function AppWindow() {
	//load dependencies
	var _ = require('/lib/underscore'),
		ui = require('/ui/components'),
		CameraHelper = require('/ui/CameraHelper');

	//create base proxy object
	var self = new ui.Window({
		navBarHidden:true,
		exitOnClose:true
	});
	self.orientationModes = [Ti.UI.PORTRAIT];
	
	self.addEventListener('open', function() {
		Ti.API.info('opening gallery');
		
		new CameraHelper().openGallery(function(blob, width, height) {
			var file = Ti.Filesystem.getFile(Ti.Filesystem.tempDirectory + Ti.Platform.createUUID() + '.png');
			file.write(blob);
			
			var Paint = require('ti.paint');
			var paintView = Paint.createPaintView({
				image: file.nativePath,
				width: '100%',
				height: '100%',
				strokeColor: '#ff0000',
				strokeWidth: 7
			});
			
			self.add(paintView);
			
			var ButtonBar = require('/ui/PlaceCancelButtonBar');
			var bar = new ButtonBar();
			self.add(bar);
			
			bar.addEventListener('done', function() {
				var annotatedImage = paintView.toImage();
				
				self.remove(paintView);
				self.remove(bar);
				
				self.add(ui.ImageView(annotatedImage));
			});
			
			bar.addEventListener('cancel', function() {
				paintView.clear();
				self.remove(paintView);
				
				paintView = Paint.createPaintView({
					image: file.nativePath,
					width: '100%',
					height: '100%',
					strokeColor: '#ff0000',
					strokeWidth: 7
				});
				self.add(paintView);
			});
		});
	});

	return self;
}

module.exports = AppWindow;