function PlaceCancelButtonBar() {
	var self = Titanium.UI.createView({
		left:0, 
		zIndex:200, 
		bottom:0, 
		width:Ti.Platform.displayCaps.platformWidth, 
		height:getHeight(10), 
		backgroundColor: '#ffffff'
	});
	
	var btnPlace = Titanium.UI.createButton({ 
		title:'Done', 
		right:10 
	});
	var btnCancel = Titanium.UI.createButton({ 
		title:L('Cancel', 'Cancel'), 
		left:10 
	});
	
	btnPlace.addEventListener('click', function() {
		self.fireEvent('done');
	});
	btnCancel.addEventListener('click', function() {
		self.fireEvent('cancel');
	});

	self.add(btnPlace);
	self.add(btnCancel);
	
	return self;

};
module.exports = PlaceCancelButtonBar;

function getWidth(val) {
	return (Ti.Platform.displayCaps.platformWidth / 100) * val;
};

function getHeight(val) {
	return (Ti.Platform.displayCaps.platformHeight / 100) * val;
};