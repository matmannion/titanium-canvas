var CameraHelper = function() {
	return this;
};

CameraHelper.prototype.takePhoto = function(callback) {
	if (!Ti.Media.isCameraSupported) {
		// We aren't going to be able to show the camera, sadly. Just open the gallery
		this.openGallery(callback);
	} else {
		var helper = this;
		
		Ti.Media.showCamera({
			success: function(evt) {
				if (evt.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
					callback(evt.media, evt.cropRect.width, evt.cropRect.height);	
				}
			},
			cancel: function(evt) {},
			error: function(error) {
				helper.openGallery(callback);
			},
			
			// A few iOS-specific properties to make it a bit nicer
			saveToPhotoGallery: false,
			allowEditing: false,
			allowImageEditing: false,
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
		});
	}
};

CameraHelper.prototype.openGallery = function(callback) {
	Ti.Media.openPhotoGallery({
		success: function(evt) {
			if (evt.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
				callback(evt.media, evt.cropRect.width, evt.cropRect.height);
			}
		},
		cancel: function(evt) {},
		error: function(error) {
			// We can't recover from this. Sorry.
		},
		
		// A few iOS-specific properties to make it a bit nicer
		allowEditing: false,
		mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO]
	});
};

module.exports = CameraHelper;